//
//  CouponQRViewController.swift
//  Obligatorio1_Alvarellos-Durán
//
//  Created by Andoni Alvarellos on 4/22/16.
//  Copyright © 2016 Andoni Alvarellos. All rights reserved.
//

import UIKit

class CouponQRViewController: UIViewController {

    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var qrImageView: UIImageView!
    
    var coupon: Coupon?
    
    override func viewDidLoad(){
        if let currentCoupon = coupon{
            discountLabel.text = String(currentCoupon.discount) + "% de descuento"
            itemImageView.image = UIImage(named: currentCoupon.imagePath)
            descriptionLabel.text = currentCoupon.description
            qrImageView.image = Utils.generateQRCodeFromString(String(currentCoupon.id))
        }
    }
}
