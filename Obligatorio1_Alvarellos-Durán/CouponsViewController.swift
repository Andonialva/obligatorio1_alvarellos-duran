//
//  CouponsViewController.swift
//  Obligatorio1_Alvarellos-Durán
//
//  Created by Andoni Alvarellos on 4/21/16.
//  Copyright © 2016 Andoni Alvarellos. All rights reserved.
//

import UIKit

class CouponsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var coupons: [Coupon] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        coupons.append(Coupon(id: 1, discount: 10, description: "Miniature Batzilla", imagePath: "Batzilla"))
        coupons.append(Coupon(id: 2, discount: 50, description: "Male T-Shirt", imagePath: "T-shirt"))
        coupons.append(Coupon(id: 1, discount: 30, description: "Space Mug", imagePath: "Mug"))
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coupons.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CouponsCellView") as! CouponsCellView
        
        let currentCoupon = coupons[indexPath.row]
        
        cell.descriptionLabel.text = currentCoupon.description
        cell.discountLabel.text = String(currentCoupon.discount) + "% de descuento"
        cell.imageView?.image = UIImage(named: currentCoupon.imagePath)
        cell.coupon = currentCoupon
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        performSegueWithIdentifier("ToCouponQRViewController", sender: tableView.cellForRowAtIndexPath(indexPath))
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ToCouponQRViewController"{
            let desController = segue.destinationViewController as! CouponQRViewController
            desController.coupon = (sender as! CouponsCellView).coupon
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
