//
//  HomeViewController.swift
//  Obligatorio1_Alvarellos-Durán
//
//  Created by Andoni Alvarellos on 4/15/16.
//  Copyright © 2016 Andoni Alvarellos. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    var refreshTimer: NSTimer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameLabel.text = "Juan"
        surnameLabel.text = "Perezoso"
        
        pointsLabel.text = "1000 points"
        NSUserDefaults.standardUserDefaults().setObject(pointsLabel.text, forKey: "points")
        
        refreshTimer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: #selector(HomeViewController.refreshPoints), userInfo: nil, repeats: true)
        
    }
    
    func refreshPoints(){
        pointsLabel.text = "1005 points"
        NSUserDefaults.standardUserDefaults().setObject(pointsLabel.text, forKey: "points")
    }
    
    @IBAction func cardButtonPressed(sender: UIButton) {
        performSegueWithIdentifier("ToCardQRViewController", sender: nil)
    }

}
