//
//  LoginViewController.swift
//  Obligatorio1_Alvarellos-Durán
//
//  Created by Andoni Alvarellos on 4/15/16.
//  Copyright © 2016 Andoni Alvarellos. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.hideKeyboard))
        view.addGestureRecognizer(tap)
        
        let swipe: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(LoginViewController.hideKeyboard))
        swipe.direction = UISwipeGestureRecognizerDirection.Up
        view.addGestureRecognizer(swipe)
    }
    
    func hideKeyboard(){
        view.endEditing(true)
    }
    
    @IBAction func signInButtonPressed(sender: UIButton) {
        var correctLogin = false
        
        if let email = emailTextField.text{
            
            if let password = passwordTextField.text{
                
                if email == "ucu@ucu.com" && password == "ucu2016"{
                    NSUserDefaults.standardUserDefaults().setObject(email, forKey: "userEmail")
                    performSegueWithIdentifier("ToTabBarController", sender: nil)
                    correctLogin = true
                }
            }
        }
        
        if !correctLogin{
            let alert = UIAlertController(title: nil, message: "Wrong Email/Password. Please try again.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }
}
