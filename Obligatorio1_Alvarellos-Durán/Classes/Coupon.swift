//
//  File.swift
//  Obligatorio1_Alvarellos-Durán
//
//  Created by Andoni Alvarellos on 4/21/16.
//  Copyright © 2016 Andoni Alvarellos. All rights reserved.
//

import Foundation

struct Coupon {
    var id: Int!
    var discount: Int!
    var description: String!
    var imagePath: String!
}

