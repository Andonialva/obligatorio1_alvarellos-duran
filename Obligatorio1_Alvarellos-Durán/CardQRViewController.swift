//
//  CardQRViewControll.swift
//  Obligatorio1_Alvarellos-Durán
//
//  Created by Andoni Alvarellos on 4/22/16.
//  Copyright © 2016 Andoni Alvarellos. All rights reserved.
//

import UIKit

class CardQRViewController: UIViewController {

    
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var qrImageView: UIImageView!
    
    override func viewDidLoad() {
        
        pointsLabel.text = NSUserDefaults.standardUserDefaults().stringForKey("points")
        qrImageView.image = Utils.generateQRCodeFromString(NSUserDefaults.standardUserDefaults().stringForKey("userEmail")!)
        
    }
    
        
}
