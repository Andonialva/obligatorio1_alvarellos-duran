//
//  CouponsCellView.swift
//  Obligatorio1_Alvarellos-Durán
//
//  Created by Andoni Alvarellos on 4/21/16.
//  Copyright © 2016 Andoni Alvarellos. All rights reserved.
//

import UIKit

class CouponsCellView: UITableViewCell {

    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var coupon: Coupon?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
